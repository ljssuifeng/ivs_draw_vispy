#!/home/ljs/anaconda/bin/python
# -*- coding=utf-8 -*-
import sys
import numpy as np
from PyQt5 import QtGui, QtCore, QtWidgets, Qt
from vispy.color.colormap import get_colormaps
from vispy import scene


'''
显示界面 gridScene
'''
class Canvas(scene.SceneCanvas):
    def __init__(self):
        scene.SceneCanvas.__init__(self, keys=None)
        self.unfreeze()
        grid = self.central_widget.add_grid()
        # Add 4 ViewBoxes to the grid
        b1 = grid.add_view(row=0, col=0, row_span=2, col_span=2)
        b1.border_color = (0.5, 0.5, 0.5, 1)
        b1.camera = scene.PanZoomCamera(rect=(-0.5, -5, 11, 10))
        # b1.camera = scene.TurntableCamera()


        # b2 = grid.add_view(row=0, col=1)
        # b2.camera = 'turntable'
        # b2.border_color = (0.5, 0.5, 0.5, 1)

        b3 = grid.add_view(row=0, col=2)
        b3.border_color = (0.5, 0.5, 0.5, 1)
        b3.camera = scene.PanZoomCamera(rect=(-10, -5, 15, 10))

        b4 = grid.add_view(row=1, col=2)
        b4.border_color = (0.5, 0.5, 0.5, 1)
        b4.camera = scene.PanZoomCamera(rect=(-5, -5, 10, 10))

        # Generate some random vertex data and a color gradient
        N = 10000
        pos = np.empty((N, 2), dtype=np.float32)
        pos[:, 0] = np.linspace(0, 10, N)
        pos[:, 1] = np.random.normal(size=N)
        pos[5000, 1] += 50

        color = np.ones((N, 4), dtype=np.float32)
        color[:, 0] = np.linspace(0, 1, N)
        color[:, 1] = color[::-1, 0]

        # Top grid cell shows plot data in a rectangular coordinate system.
        l1 = scene.visuals.Line(pos=pos, color=color, antialias=False, method='gl')
        b1.add(l1)
        grid1 = scene.visuals.GridLines(parent=b1.scene)

        # Bottom-left grid cell shows the same data with log-transformed X
        # grid2 = scene.visuals.GridLines(parent=b2.scene)

        # Bottom-left grid cell shows the same data with log-transformed X
        e2 = scene.Node(parent=b3.scene)
        e2.transform = scene.transforms.LogTransform(base=(2, 0, 0))
        l2 = scene.visuals.Line(pos=pos, color=color, antialias=False, parent=e2,
                                method='gl')
        grid3 = scene.visuals.GridLines(parent=e2)

        # Bottom-right grid cell shows the same data again, but with a much more
        # interesting transformation.
        e3 = scene.Node(parent=b4.scene)
        affine = scene.transforms.MatrixTransform()
        affine.scale((1, 0.1))
        affine.rotate(10, (0, 0, 1))
        affine.translate((0, 1))
        e3.transform = scene.transforms.ChainTransform([
            scene.transforms.PolarTransform(),
            affine])
        l3 = scene.visuals.Line(pos=pos, color=color, antialias=False, parent=e3,
                                method='gl')
        grid4 = scene.visuals.GridLines(scale=(np.pi / 6., 1.0), parent=e3)

        self.freeze()

        self.show()


'''
Qt 交互设置界面
'''
class ObjectWidget(QtWidgets.QWidget):
    """
    Widget for editing OBJECT parameters
    """
    signal_objet_changed = QtCore.pyqtSignal(name='objectChanged')

    def __init__(self, parent=None):
        super(ObjectWidget, self).__init__(parent)

        l_nbr_steps = QtWidgets.QLabel("Nbr Steps ")
        self.nbr_steps = QtWidgets.QSpinBox()
        self.nbr_steps.setMinimum(3)
        self.nbr_steps.setMaximum(100)
        self.nbr_steps.setValue(6)
        self.nbr_steps.valueChanged.connect(self.update_param)

        l_cmap = QtWidgets.QLabel("Cmap ")
        self.cmap = list(get_colormaps().keys())
        self.combo = QtWidgets.QComboBox(self)
        self.combo.addItems(self.cmap)
        self.combo.currentIndexChanged.connect(self.update_param)

        gbox = QtWidgets.QGridLayout()
        gbox.addWidget(l_cmap, 0, 0)
        gbox.addWidget(self.combo, 0, 1)
        gbox.addWidget(l_nbr_steps, 1, 0)
        gbox.addWidget(self.nbr_steps, 1, 1)

        vbox = QtWidgets.QVBoxLayout()
        vbox.addLayout(gbox)
        vbox.addStretch(1.0)

        self.setLayout(vbox)

    def update_param(self, option):
        self.signal_objet_changed.emit()

'''
主界面
'''
class MainWindow(QtWidgets.QMainWindow):

    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)

        self.resize(Qt.QApplication.desktop().availableGeometry().width(),
                    Qt.QApplication.desktop().availableGeometry().height())

        self.setWindowTitle('IVS_view')
        self.setWindowIcon(QtGui.QIcon('radar.jpg'))

        # palette = QtGui.QPalette()
        # palette.setColor(QtGui.QPalette.Background,Qt.QColor(155,155,160,55))
        # self.setPalette(palette)

        splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)

        self.props = ObjectWidget()
        self.canvas = Canvas()
        self.canvas.create_native()
        self.canvas.native.setParent(self)
        splitter.addWidget(self.props)
        splitter.addWidget(self.canvas.native)

        splitter.setStretchFactor(0,1)
        splitter.setStretchFactor(1,3)
        splitter.setHandleWidth(1)

        self.setCentralWidget(splitter)
        self.props.signal_objet_changed.connect(self.update_view)
        self.update_view()

    def update_view(self):
        pass


if __name__ == '__main__':
    appQt = Qt.QApplication(sys.argv)
    win = MainWindow()
    win.show()
    appQt.exec_()