from vispy import scene, app, gloo
from PyQt5 import QtGui, QtCore, QtWidgets, Qt
import numpy as np
import sys

from vispy.color.colormap import get_colormaps
from vispy.gloo import Program, VertexBuffer, IndexBuffer
from vispy.util.transforms import perspective, translate, rotate
from vispy.geometry import create_cube
from vispy.geometry.generation import create_sphere
from threading import Thread
import queue
from Camera import CameraView, Camera_receive
from Radar import EsrSensor, ESRView


class ObjectWidget(QtWidgets.QWidget):
    """
    Widget for editing OBJECT parameters
    """
    signal_objet_changed = QtCore.pyqtSignal(name='objectChanged')

    def __init__(self, parent=None):
        super(ObjectWidget, self).__init__(parent)

        l_nbr_steps = QtWidgets.QLabel("Nbr Steps ")
        self.nbr_steps = QtWidgets.QSpinBox()
        self.nbr_steps.setMinimum(3)
        self.nbr_steps.setMaximum(100)
        self.nbr_steps.setValue(6)
        self.nbr_steps.valueChanged.connect(self.update_param)

        l_cmap = QtWidgets.QLabel("Cmap ")
        self.cmap = list(get_colormaps().keys())
        self.combo = QtWidgets.QComboBox(self)
        self.combo.addItems(self.cmap)
        self.combo.currentIndexChanged.connect(self.update_param)

        gbox = QtWidgets.QGridLayout()
        gbox.addWidget(l_cmap, 0, 0)
        gbox.addWidget(self.combo, 0, 1)
        gbox.addWidget(l_nbr_steps, 1, 0)
        gbox.addWidget(self.nbr_steps, 1, 1)

        vbox = QtWidgets.QVBoxLayout()
        vbox.addLayout(gbox)
        vbox.addStretch(1.0)

        self.setLayout(vbox)

    def update_param(self, option):
        self.signal_objet_changed.emit()

class Lida(scene.SceneCanvas):
    def __init__(self):
        scene.SceneCanvas.__init__(self, keys=None)
        self.unfreeze()
        self.view = self.central_widget.add_view()
        self.radius = 2.0
        self.view.camera = 'turntable'
        mesh = create_sphere(20, 20, radius=self.radius)
        vertices = mesh.get_vertices()
        tris = mesh.get_faces()

        cl = np.linspace(-self.radius, self.radius, 6 + 2)[1:-1]

        self.iso = scene.visuals.Isoline(vertices=vertices, tris=tris,
                                         data=vertices[:, 2],
                                         levels=cl, color_lev='autumn',
                                         parent=self.view.scene)
        self.freeze()

        # Add a 3D axis to keep us oriented
        scene.visuals.XYZAxis(parent=self.view.scene)

    def set_data(self, n_levels, cmap):
        self.iso.set_color(cmap)
        cl = np.linspace(-self.radius, self.radius, n_levels + 2)[1:-1]
        self.iso.levels = cl

class Radar(scene.SceneCanvas):
    def __init__(self):
        scene.SceneCanvas.__init__(self, keys=None)
        self.unfreeze()
        view = self.central_widget.add_view()
        # title = scene.visuals.Text('Radar', parent=view.scene, color='ivory',
        #                            pos=(40, 20), font_size=18)

        self.freeze()
    def set_size(self, width, height):
        self.size = (width, height)


class MainWindow(QtWidgets.QMainWindow):

    def __init__(self):
        QtWidgets.QMainWindow.__init__(self)

        self.resize(Qt.QApplication.desktop().availableGeometry().width(),
                    Qt.QApplication.desktop().availableGeometry().height())

        self.setWindowTitle('IVS_view')
        self.setWindowIcon(QtGui.QIcon('radar.jpg'))

        # palette = QtGui.QPalette()
        # palette.setColor(QtGui.QPalette.Background,Qt.QColor(155,155,160,55))
        # self.setPalette(palette)

        splitter = QtWidgets.QSplitter(QtCore.Qt.Horizontal)
        splitter_v = QtWidgets.QSplitter(QtCore.Qt.Vertical)

        self.props = ObjectWidget()
        splitter.addWidget(self.props)


        self.lida = Lida()
        self.lida.create_native()
        self.lida.native.setParent(self)

        self.radar = ESRView()
        self.radar.set_size(480, 400)
        self.radar.create_native()
        self.radar.native.setParent(self)

        self.camera = CameraView()
        self.camera.set_size(480, 400)
        self.camera.create_native()
        self.camera.native.setParent(self)

        splitter.addWidget(self.lida.native)
        splitter_v.addWidget(self.radar.native)
        splitter_v.addWidget(self.camera.native)
        splitter.addWidget(splitter_v)

        splitter_v.setStretchFactor(0, 1)
        splitter_v.setStretchFactor(1, 1)
        splitter_v.setHandleWidth(1)
        # splitter_v.setAutoFillBackground(True)

        splitter.setStretchFactor(0,1)
        splitter.setStretchFactor(1,1)
        splitter.setStretchFactor(2,1)
        splitter.setHandleWidth(1)
        # splitter.setAutoFillBackground(True)

        self.setCentralWidget(splitter)
        self.props.signal_objet_changed.connect(self.update_view)
        self.update_view()

    def update_view(self):
        # # banded, nbr_steps, cmap
        self.lida.set_data(self.props.nbr_steps.value(),
                             self.props.combo.currentText())
        # pass




if __name__ == '__main__':
    picture = np.array([], dtype=np.uint8)
    camera_receive = Camera_receive("camera_data")
    camera_receive.start()
    esr_rear = EsrSensor("ESR_REAR_RAW_DATA")
    esr_rear.start()


    appQt = Qt.QApplication(sys.argv)
    win = MainWindow()
    win.show()
    appQt.exec_()



