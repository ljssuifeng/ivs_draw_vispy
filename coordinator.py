# /usr/bin/python/
# coding:utf-8

#######################################################
#
#     定义坐标变换函数
#                                         lmx  2016.1.6
#######################################################

import numpy as np


# 行向量点计算法
def MatrixR_RX(rad_angle):
    return np.array([[1, 0, 0],
                     [0, np.cos(rad_angle), -np.sin(rad_angle)],
                     [0, np.sin(rad_angle), np.cos(rad_angle)]])


def MatrixR_RY(rad_angle):
    return np.array([[np.cos(rad_angle), 0, np.sin(rad_angle)],
                     [0, 1, 0],
                     [-np.sin(rad_angle), 0, np.cos(rad_angle)]])


def MatrixR_RZ(rad_angle):
    return np.array([[np.cos(rad_angle), -np.sin(rad_angle), 0],
                     [np.sin(rad_angle), np.cos(rad_angle), 0],
                     [0, 0, 1]])


# 列向量点计算法
def MatrixC_RX(rad_angle):
    return np.array([[1, 0, 0],
                     [0, np.cos(rad_angle), np.sin(rad_angle)],
                     [0, -np.sin(rad_angle), np.cos(rad_angle)]])


def MatrixC_RY(rad_angle):
    return np.array([[np.cos(rad_angle), 0, -np.sin(rad_angle)],
                     [0, 1, 0],
                     [np.sin(rad_angle), 0, np.cos(rad_angle)]])


def MatrixC_RZ(rad_angle):
    return np.array([[np.cos(rad_angle), np.sin(rad_angle), 0],
                     [-np.sin(rad_angle), np.cos(rad_angle), 0],
                     [0, 0, 1]])


def main():
    pass


if __name__ == '__main__':
    main()
