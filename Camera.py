from vispy import gloo, scene, app
from threading import Thread
from lcmtypes import camera_t
import cv2, lcm, queue
import numpy as np

picture = np.array([], dtype=np.uint8)
que_picture = queue.Queue(20)

picture_vertex = """
    attribute vec2 position;
    attribute vec2 texcoord;
    varying vec2 v_texcoord;
    void main()
    {
        gl_Position = vec4(position, 0.0, 1.0);
        v_texcoord = texcoord;
    }
"""

picture_fragment = """
    uniform sampler2D texture;
    varying vec2 v_texcoord;
    void main()
    {
        gl_FragColor = texture2D(texture, v_texcoord);
    }
"""

class CameraView(scene.SceneCanvas):
    def __init__(self):
        scene.SceneCanvas.__init__(self, keys=None)
        self.unfreeze()
        view = self.central_widget.add_view()
        self.pic_program = gloo.Program(picture_vertex, picture_fragment)
        self.pic_program['position'] = [(-1, -1), (-1, +1), (+1, -1), (+1, +1)]
        self.pic_program['texcoord'] = [(0, 1), (0, 0), (1, 1), (1, 0)]
        self.pic_program['texture'] = np.zeros((480, 640, 3), np.uint8)
        self._timer = app.Timer('auto', connect=self.on_timer, start=True)

        self.freeze()

    def set_size(self, width, height):
        self.size = (width, height)

    def on_draw(self, event):
        if not que_picture.empty():
            pic_show = que_picture.get()
            self.pic_program['texture'] = np.zeros(pic_show.shape, np.uint8)
            (r, g, b) = cv2.split(pic_show)
            img = cv2.merge([b, g, r])
            self.pic_program['texture'] = img
        self.pic_program.draw(gloo.gl.GL_TRIANGLE_STRIP)

    def on_timer(self, event):
        self.update()

class Camera_receive(Thread):
    def __init__(self,receive_label):
        Thread.__init__(self)
        self.camera = camera_t()
        self.lcm_receive_label = receive_label
        self.lc = lcm.LCM("udpm://239.255.76.67:7667?ttl=1")
        self.setDaemon(True)

        self.subscription = self.lc.subscribe(self.lcm_receive_label, self.camera_receive_handler)

    def camera_receive_handler(self, channel, data):
        global picture
        da = camera_t.decode(data)
        fragmentCount = da.fragmentCount
        fragmentIndex = da.fragmentIndex
        fragmentData = da.data
        imageSize = da.imageSize
        if fragmentIndex == 0:
            picture = np.array([], dtype=np.uint8)
        picture = np.concatenate((picture, np.array(bytearray(fragmentData))))
        if fragmentIndex == fragmentCount - 1:
            '''解码显示'''
            if imageSize == picture.__len__():
                pic_show = cv2.imdecode(picture, 1)
                que_picture.put(pic_show)


    def run(self):
        try:
            while True:
                self.lc.handle()
        except KeyboardInterrupt:
            print ("function run error")


if __name__ == '__main__':
    win = CameraView()
    win.set_size(800,600)
    win.show()
    camera_receive = Camera_receive("camera_data")
    camera_receive.start()
    app.run()