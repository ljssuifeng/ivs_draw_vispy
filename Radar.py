from vispy import gloo, scene, app
from threading import Thread
from lcmtypes import objects_t, esr_raw_data_t, point_obj_t
import lcm, queue
import numpy as np
import threading
import coordinator, math, struct

que_esr_front = queue.Queue(20)
que_esr_rear = queue.Queue(20)

VERT_SHADER = """
attribute vec2 a_position;
uniform float u_size;

void main() {
    gl_Position = vec4(a_position, 0.0, 1.0);
    gl_PointSize = u_size;
}
"""

FRAG_SHADER = """
void main() {
    gl_FragColor = vec4(1.0, 0.0, 0.0, 1.0);
}
"""


class EsrSensor(object):
    def __init__(self, raw_data_name):
        self.who_receive = raw_data_name
        self.lc = lcm.LCM("udpm://239.255.76.67:7667?ttl=1")
        # self.lc = lcm.LCM()
        self.subscription = self.lc.subscribe(self.who_receive, self.Esr_handler)
        self.objects = objects_t()

        self.objectangleintmask = int('1' * 10, 2)
        self.objectrangeintmask = int('1' * 11, 2)
        self.objectspeedintmask = int('1' * 14, 2)
        self.objectdata = np.zeros([64, 14])  # 障碍目标缓冲[角度，距离，速度，x,y,z,x0,y0,z0,count,lat,lon,head,obj_id]
        self.validobjectdata = self.objectdata  # 识别跟踪到的有效目标

        # 雷达安装位置坐标系相对整车坐标系的变换参数
        self.xoffset, self.yoffset, self.zoffset = 0, 0, 0
        self.pitch, self.yaw, self.roll = 0, 0, 0
        if self.who_receive == "ESR_FRONT_RAW_DATA":
            self.yaw = 0
            self.xoffset = 0
            self.yoffset = 4
        if self.who_receive == "ESR_REAR_RAW_DATA":
            self.yaw = 180
            self.xoffset = 0
            self.yoffset = -1

        self.set_Rm()
        self.set_Tm()

        self.load_record_data_Flag = False
        self.load_record_csv_Flag = False
        self.record_file_name = None

    def start(self):
        esr_thread = threading.Thread(target=self.run,name=self.who_receive)
        esr_thread.daemon = True
        esr_thread.start()

    def run(self):
        try:
            while True:
                self.lc.handle()
        except KeyboardInterrupt:
            pass

    def set_Rm(self):
        self.Rx = coordinator.MatrixR_RX(math.radians(self.pitch))
        self.Ry = coordinator.MatrixR_RY(math.radians(self.roll))
        self.Rz = coordinator.MatrixR_RZ(math.radians(self.yaw))

        self.RM = np.dot(np.dot(self.Rx, self.Ry), self.Rz)

    def set_Tm(self):

        self.TM = np.array([self.xoffset, self.yoffset, self.zoffset])

    def Esr_handler(self,channel,data):
        msg = esr_raw_data_t.decode(data)
        self.msg_decode(msg.do_msg_id, msg.do_frame.data)

    def msg_decode(self, msg_id, msg_data):
        data_index = msg_id - 0x500
        if 0 <= data_index < 64:
            framedataint, = struct.unpack(">Q", msg_data)
            objectangleint = (framedataint >> 43) & self.objectangleintmask
            objectrangeint = (framedataint >> 32) & self.objectrangeintmask
            objectspeedint = (framedataint >> 0) & self.objectspeedintmask

            self.objectdata[data_index, 13] = data_index    #obj_id

            if 0 == objectangleint & 0 == objectrangeint:
                # 如果角度和距离都为0 则为无效数据,没有识别到有效障碍，该索引缓冲清零
                self.objectdata[data_index, :] = 0
            else:
                # 如果角度或距离不为0 则为有效数据,识别到有效障碍，该索引 障碍计数累加1
                self.objectdata[data_index, 9] += 1

                if 512 == objectangleint & 512:
                    objectangleint = -(~(objectangleint - 1) & 0x3ff)
                #print 'angle', objectangleint*0.1

                if 8192 == objectspeedint & 8192:
                    objectspeedint = -(~(objectspeedint - 1) & 0x3fff)
                self.objectdata[data_index, 0] = objectangleint * 0.1  # angle ° -51.2 to 51.1  度
                self.objectdata[data_index, 1] = objectrangeint * 0.1  # distance m 0-204.7 m
                self.objectdata[data_index, 2] = objectspeedint * 0.01  # speed m/s -81.92 to 81.91 m/s

        if data_index == 63:
            # 收到一帧数据
            # 雷达坐标系下 x y z 坐标变换前的位置
            np.deg2rad(self.objectdata[:, 0], self.objectdata[:, 0])
            self.objectdata[:, 3] = self.objectdata[:, 1] * np.sin(self.objectdata[:, 0])
            self.objectdata[:, 4] = self.objectdata[:, 1] * np.cos(self.objectdata[:, 0])
            self.objectdata[data_index, 5] = 0

            # 整车坐标系下 x y z 即坐标变换后的位置
            self.objectdata[:, 6:9] = np.dot(self.objectdata[:, 3:6], self.RM) + self.TM
            # 识别到的有效障碍，条件：连续识别次数大于n=10
            self.validobjectdata = self.objectdata[self.objectdata[:, 9] > 2]  #

            self.objects.objects.clear()
            for data in self.validobjectdata:
                key = point_obj_t()
                key.x,key.y, key.angle = data[6],data[7], data[2]
                self.objects.objects.append(key)
            self.objects.object_number = self.objects.objects.__len__()

            if self.who_receive == "ESR_FRONT_RAW_DATA":
                que_esr_front.put(self.objects)
            elif self.who_receive == "ESR_REAR_RAW_DATA":
                que_esr_rear.put(self.objects)

class ESRView(scene.SceneCanvas):
    def __init__(self):
        scene.SceneCanvas.__init__(self)
        self.size = (480,400)
        self.unfreeze()
        ps = self.pixel_scale
        v_position = np.array([[0,0],[0,0]]).astype(np.float32)
        # v_position = 0.25 * np.random.randn(200, 2).astype(np.float32)
        self.program = gloo.Program(VERT_SHADER, FRAG_SHADER)
        self.program['a_position'] = gloo.VertexBuffer(v_position)
        self.program['u_size'] = 10. * ps
        self._timer = app.Timer('auto', connect=self.on_timer, start=True)
        self.freeze()

        self.show()


    def on_resize(self, event):
        width, height = event.size
        gloo.set_viewport(0, 0, width, height)

    def set_size(self, width, height):
        self.size = (width, height)
        gloo.set_viewport(0, 0, width, height)

    def on_draw(self, event):
        objects = np.array([], dtype=np.float32)
        if not que_esr_front.empty():
            esr_front = que_esr_front.get()
            for data in esr_front.objects:
                objects = np.append(objects, [data.x, data.y, data.angle])
        if not que_esr_rear.empty():
            esr_rear = que_esr_rear.get()
            for data in esr_rear.objects:
                objects = np.append(objects, [data.x, data.y, data.angle])

        if objects.__len__() > 0:
            # objs = 0.25 * np.random.uniform(0,1,((int)(objects.__len__() / 3), 3)).astype(np.float32)
            objs = objects.reshape((int)(objects.__len__() / 3), 3).astype(np.float32)
            self.program['a_position'] = gloo.VertexBuffer(0.01*objs[:, :2])

        gloo.clear('black')
        self.program.draw('points')


    def on_timer(self, event):
        gloo.set_viewport(0, 0, *self.physical_size)
        self.update()


if __name__ == '__main__':
    esr_front = EsrSensor("ESR_FRONT_RAW_DATA")
    esr_front.start()
    esr_rear = EsrSensor("ESR_REAR_RAW_DATA")
    esr_rear.start()
    win = ESRView()
    win.set_size(800,600)
    win.show()

    app.run()